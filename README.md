# Randary - Flutter

Random C/C++ arrays generator. This tool allows you to generate pseudo-random arrays of integers and format them in C/C++ style. It is written in Dart language using Flutter.

You can see the result here: https://slebe.dev/randary-flutter/#/