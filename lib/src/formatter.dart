import 'data_type.dart';

String format(List<BigInt> data, DataType type) {
  var result = formatAsArray(data, type, ArrayRadix.dec);
  result += "\n";
  result += formatAsArray(data, type, ArrayRadix.hex);
  if (type == DataType.uint8) result += "\n${formatBytesAsString(data)}";
  return result;
}

enum ArrayRadix { dec, hex }

int numberPerLine(DataType type, ArrayRadix arrayRadix) {
  if (arrayRadix == ArrayRadix.dec) {
    switch (type) {
      case DataType.uint8:
        return 14;
      case DataType.uint16:
        return 10;
      case DataType.uint32:
        return 6;
      case DataType.uint64:
        return 4;
      default:
        throw UnimplementedError();
    }
  } else if (arrayRadix == ArrayRadix.hex) {
    switch (type) {
      case DataType.uint8:
        return 12;
      case DataType.uint16:
        return 9;
      case DataType.uint32:
        return 6;
      case DataType.uint64:
        return 3;
      default:
        throw UnimplementedError();
    }
  } else {
    throw UnimplementedError();
  }
}

String formatAsArray(List<BigInt> data, DataType type, ArrayRadix arrayRadix) {
  var groupBy = numberPerLine(type, arrayRadix);
  var cType = cTypeName(type);
  var indentString = "    ";
  String numPrefix;
  String nameSuffix;
  int radix;
  int padding;
  String paddingSymbol;
  switch (arrayRadix) {
    case ArrayRadix.dec:
      numPrefix = "";
      nameSuffix = "dec";
      radix = 10;
      switch (type) {
        case DataType.uint8:
          padding = 3;
          break;
        case DataType.uint16:
          padding = 5;
          break;
        case DataType.uint32:
          padding = 10;
          break;
        case DataType.uint64:
          padding = 20;
          break;
      }
      paddingSymbol = ' ';
      break;
    case ArrayRadix.hex:
      numPrefix = "0x";
      nameSuffix = "hex";
      radix = 16;
      padding = 2 * dataTypeSize(type);
      paddingSymbol = '0';
      break;
  }

  var result =
      "unsigned $cType array_$nameSuffix[${data.length}u] = {\n$indentString";
  for (var i = 0; i < data.length; ++i) {
    var numStr = data[i].toRadixString(radix).toUpperCase();

    if (padding > 0) numStr = numStr.padLeft(padding, paddingSymbol);

    result += "$numPrefix$numStr";
    if (i != data.length - 1) {
      if (i % groupBy == groupBy - 1)
        result += ",\n$indentString";
      else
        result += ", ";
    }
  }
  result += "\n};";
  return result;
}

String formatBytesAsString(List<BigInt> data) {
  var groupBy = 16;
  var indentString = "    ";
  var result =
      "unsigned char string_hex[${data.length + 1}u] = \n"; //+1 for null-terminator
  for (var i = 0; i < data.length; ++i) {
    if (i % groupBy == 0) result += "$indentString\"";
    result += "\\x${data[i].toRadixString(16).toUpperCase().padLeft(2, '0')}";
    if (i == data.length - 1)
      result += "\";";
    else if (i % groupBy == groupBy - 1) result += "\"\n";
  }
  return result;
}
