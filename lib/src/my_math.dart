BigInt max(BigInt left, BigInt right) {
  return left > right ? left : right;
}

BigInt min(BigInt left, BigInt right) {
  return left < right ? left : right;
}

///Power 2^exponent
BigInt pow2(int exponent) {
  return BigInt.from(1) << exponent;
}