enum DataType { uint8, uint16, uint32, uint64 }

int dataTypeSize(DataType type) {
  switch(type) {
    case DataType.uint8:
      return 1;
    case DataType.uint16:
      return 2;
    case DataType.uint32:
      return 4;
    case DataType.uint64:
      return 8;
    default: 
      throw UnimplementedError();
  }
}

String cTypeName(DataType type) {
  switch(type) {
    case DataType.uint8:
      return "char";
    case DataType.uint16:
      return "short";
    case DataType.uint32:
      return "int";
    case DataType.uint64:
      return "long long";
    default: 
      throw UnimplementedError();
  }
}

BigInt maxValue(DataType type) {
  switch(type) {
    case DataType.uint8:
      return BigInt.from(0xFF);
    case DataType.uint16:
      return BigInt.from(0xFFFF);
    case DataType.uint32:
      return BigInt.from(0xFFFFFFFF);
    case DataType.uint64:
      return (BigInt.from(0xFFFFFFFF) << 32) | BigInt.from(0xFFFFFFFF);
    default: 
      throw UnimplementedError();
  }
}