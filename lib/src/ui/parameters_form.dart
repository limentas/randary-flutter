import 'dart:math';
import 'package:flutter/material.dart';
import 'package:randary_flutter/src/data_type.dart';
import 'package:randary_flutter/src/my_math.dart' as my_math;

class ParametersForm extends StatefulWidget {
  ParametersForm({Key key}) : super(key: key);

  @override
  ParametersFormState createState() {
    return ParametersFormState();
  }
}

class ParametersFormState extends State<ParametersForm> {
  ParametersFormState() {
    _lowerBoundController = TextEditingController(text: "$lowerBound");
    _upperBoundController = TextEditingController(text: "$upperBound");
  }

  final formKey = GlobalKey<FormState>();

  final int maxArraySize = pow(2, 32);
  final sizeController = TextEditingController(text: "16");
  var _lowerBoundController;
  var _upperBoundController;
  BigInt lowerBound = BigInt.from(0);
  BigInt upperBound = BigInt.from(0xFF);
  BigInt _maxDataValue = BigInt.from(0xFF);

  void updateDateType(DataType dataType) {
    setState(() {
      var newMaxValue = maxValue(dataType);
      if (newMaxValue <= lowerBound) {
        lowerBound = BigInt.from(0);
        _lowerBoundController.text = "$lowerBound";
      }
      if (newMaxValue < upperBound) {
        upperBound = _maxDataValue;
        _upperBoundController.text = "$upperBound";
      }
      if (upperBound == _maxDataValue) {
        upperBound = newMaxValue;
        _upperBoundController.text = "$upperBound";
      }
      _maxDataValue = newMaxValue;
    });
  }

  void dispose() {
    sizeController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the formKey created above.
    return Form(
        key: formKey,
        autovalidate: true,
        child: Column(children: <Widget>[
          TextFormField(
              controller: sizeController,
              style: Theme.of(context).textTheme.headline6,
              keyboardType: TextInputType.numberWithOptions(
                  signed: false, decimal: false),
              decoration: const InputDecoration(
                  hintText: "Count of elements in result array",
                  labelText: "Result array size"),
              validator: (String value) {
                var size = int.tryParse(value);
                if (size == null) return "Only integer numbers are valid";
                if (size <= 0) return "Only positive numbers are valid";
                if (size > maxArraySize)
                  return "Number should be not more than $maxArraySize";
                return null;
              }),
          const SizedBox(height: 20),
          SliderTheme(
              data: const SliderThemeData(
                  showValueIndicator: ShowValueIndicator.always,
                  minThumbSeparation: 1),
              child: RangeSlider(
                  values:
                      RangeValues(lowerBound.toDouble(), upperBound.toDouble()),
                  min: 0,
                  max: _maxDataValue.toDouble(),
                  labels:
                      RangeLabels(lowerBound.toString(), upperBound.toString()),
                  onChanged: (value) {
                    setState(() {
                      lowerBound = my_math.max(BigInt.from(value.start.round()), BigInt.from(0));
                      upperBound = my_math.min(BigInt.from(value.end.round()), _maxDataValue);
                      _lowerBoundController.text = "$lowerBound";
                      _upperBoundController.text = "$upperBound";
                    });
                  })),
          Row(children: <Widget>[
            Flexible(
                child: TextFormField(
                    controller: _lowerBoundController,
                    style: Theme.of(context).textTheme.headline6,
                    keyboardType:
                        TextInputType.numberWithOptions(signed: false),
                    decoration: const InputDecoration(
                        hintText: "Natural number",
                        labelText: "Min generated value"),
                    validator: (String value) {
                      var bound = BigInt.tryParse(value);
                      if (bound == null)
                        return "Only integer numbers are valid";
                      if (bound < BigInt.from(0))
                        return "Only non-negative numbers are valid";
                      if (bound >= upperBound)
                        return "Min should be less than Max";
                      return null;
                    },
                    onEditingComplete: () {
                      if (formKey.currentState.validate()) {
                        setState(() {
                          lowerBound =
                              BigInt.tryParse(_lowerBoundController.text);
                        });
                      }
                    })),
            Flexible(
                child: TextFormField(
                    controller: _upperBoundController,
                    style: Theme.of(context).textTheme.headline6,
                    keyboardType:
                        TextInputType.numberWithOptions(signed: false),
                    decoration: const InputDecoration(
                        hintText: "Natural number",
                        labelText: "Max generated value"),
                    validator: (String value) {
                      var bound = BigInt.tryParse(value);
                      if (bound == null)
                        return "Only integer numbers are valid";
                      if (bound <= lowerBound)
                        return "Max should be greater than Min";
                      if (bound > _maxDataValue)
                        return "Number should be not more than $_maxDataValue";
                      return null;
                    },
                    onEditingComplete: () {
                      if (formKey.currentState.validate()) {
                        setState(() {
                          upperBound =
                              BigInt.tryParse(_upperBoundController.text);
                        });
                      }
                    })),
          ])
        ]));
  }
}
