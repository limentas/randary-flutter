import 'package:flutter/material.dart';
import '../data_type.dart';


class DataTypeChoise extends StatefulWidget {
  DataTypeChoise(
      {Key key, this.defaultValue: DataType.uint8, this.onChanged})
      : super(key: key);

  final DataType defaultValue;
  final ValueChanged<DataType> onChanged;

  @override
  DataTypeChoiseState createState() => DataTypeChoiseState(this.defaultValue);
}

class DataTypeChoiseState extends State<DataTypeChoise> {
  DataTypeChoiseState(DataType defaultValue):
    selectedValue = defaultValue;
  
  DataType selectedValue;

  void _handleRadioToggle(DataType value) {
    setState(() {
      selectedValue = value;
    });
    widget.onChanged(value);
  }

  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        RadioListTile<DataType>(
          title: const Text('unsigned byte'),
          value: DataType.uint8,
          groupValue: selectedValue,
          onChanged: (DataType value) {
            _handleRadioToggle(value);
          },
        ),
        RadioListTile<DataType>(
          title: const Text('unsigned int16'),
          value: DataType.uint16,
          groupValue: selectedValue,
          onChanged: (DataType value) {
            _handleRadioToggle(value);
          },
        ),
        RadioListTile<DataType>(
          title: const Text('unsigned int32'),
          value: DataType.uint32,
          groupValue: selectedValue,
          onChanged: (DataType value) {
            _handleRadioToggle(value);
          },
        ),
        RadioListTile<DataType>(
          title: const Text('unsigned int64'),
          value: DataType.uint64,
          groupValue: selectedValue,
          onChanged: (DataType value) {
            _handleRadioToggle(value);
          },
        ),
      ],
    );
  }
}
