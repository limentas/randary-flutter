import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'dart:ui';

class OutputView extends StatefulWidget {
  OutputView({Key key}) : super(key: key);

  @override
  OutputViewState createState() {
    return OutputViewState();
  }
}

class OutputViewState extends State<OutputView> {
  String _resultString = "";

  void updateResult(String newValue) {
    setState(() {
      _resultString = newValue;
    });
  }

  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the formKey created above.
    return Visibility(
        visible: _resultString.isNotEmpty,
        child: Stack(children: <Widget>[
          Card(
              margin: const EdgeInsets.all(20),
              child: Padding(
                  padding: const EdgeInsets.all(10),
                  child: SelectableText(_resultString,
                      enableInteractiveSelection: true,
                      toolbarOptions:
                          ToolbarOptions(copy: true, selectAll: true),
                      style: const TextStyle(
                        fontSize: 16,
                        fontFeatures: [FontFeature.tabularFigures()],
                        fontFamily: "monospace",
                      )))),
          Positioned(
              right: 30,
              top: -10,
              child: ButtonTheme(
                  minWidth: 50,
                  height: 20,
                  child: FlatButton(
                      color: Theme.of(context).accentColor,
                      shape: const RoundedRectangleBorder(),
                      padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 8),
                      textColor: Theme.of(context).accentTextTheme.button.color,
                      child: const Text("Copy all"),
                      onPressed: () {
                        Clipboard.setData(ClipboardData(text: _resultString));
                      })))
        ]));
  }
}
