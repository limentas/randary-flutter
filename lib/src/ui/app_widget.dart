import 'package:flutter/material.dart';
import 'main_view.dart';

class AppWidget extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Randary - Flutter',
      theme: ThemeData(
        // This is the theme of your application.
        primaryColor: Colors.blueGrey[800],
        accentColor: Colors.grey[800],
        buttonColor: Colors.grey[800],
      ),
      home: MainView(title: 'Randary - Flutter'),
    );
  }
}
