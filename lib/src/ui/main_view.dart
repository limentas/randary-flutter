import 'package:flutter/material.dart';
import 'package:randary_flutter/src/ui/data_type_choise.dart';
import 'package:randary_flutter/src/ui/parameters_form.dart';
import 'package:randary_flutter/src/ui/output_view.dart';
import 'package:randary_flutter/src/generator.dart';
import 'package:randary_flutter/src/formatter.dart';
import 'package:randary_flutter/src/data_type.dart';

class MainView extends StatefulWidget {
  MainView({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MainViewState createState() => _MainViewState();
}

class _MainViewState extends State<MainView> {
  final _dataTypeChoiseKey = GlobalKey<DataTypeChoiseState>();
  final _parametersFormKey = GlobalKey<ParametersFormState>();
  final _outputViewKey = GlobalKey<OutputViewState>();

  void _generate() {
    setState(() {
      var formState = _parametersFormKey.currentState;
      if (formState.formKey.currentState.validate()) {
        var data = generateData(int.parse(formState.sizeController.text),
            formState.lowerBound, formState.upperBound);
        var resultString =
            format(data, _dataTypeChoiseKey.currentState.selectedValue);
        _outputViewKey.currentState.updateResult(resultString);
      } else {
        _outputViewKey.currentState.updateResult("");
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
        appBar: AppBar(
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
          title: Text(widget.title),
        ),
        body: LayoutBuilder(builder:
            (BuildContext context, BoxConstraints viewportConstraints) {
          return Scrollbar( child: SingleChildScrollView(
              child: ConstrainedBox(
              constraints: BoxConstraints(
                minHeight: viewportConstraints.maxHeight,
              ),
              // Center is a layout widget. It takes a single child and positions it
              // in the middle of the parent.
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Flexible(
                            flex: 1,
                            child: ConstrainedBox(
                              constraints: const BoxConstraints(
                                  maxWidth: 300, maxHeight: 240),
                              child: DataTypeChoise(
                                  key: _dataTypeChoiseKey,
                                  defaultValue: DataType.uint8,
                                  onChanged: (dataType) {
                                    _parametersFormKey.currentState
                                        .updateDateType(dataType);
                                  }),
                            )),
                        Flexible(
                            flex: 2,
                            child: ConstrainedBox(
                                constraints: const BoxConstraints(
                                    maxWidth: 480, maxHeight: 240),
                                child:
                                    ParametersForm(key: _parametersFormKey))),
                      ]),
                  RaisedButton(
                      onPressed: _generate,
                      textColor: Theme.of(context).accentTextTheme.button.color,
                      child: const Text('Generate',
                          style: TextStyle(fontSize: 24))),
                  OutputView(key: _outputViewKey),
                ],
              ),
            ),),
          );
        }));
  }
}
