import 'dart:math';
import 'package:randary_flutter/src/my_math.dart' as my_math;

List<BigInt> generateData(int size, BigInt lowerBound, BigInt upperBound) {
  final maxValue = upperBound - lowerBound;
  var result = List<BigInt>(size);

  // I think this is not the best algorithm, but it allows to get uniformly distributed sequences.
  // Main point is using modulo to limit results on top, so we need to drop candidates from the end, otherwise
  // numbers from (0, maxGeneratedCandidate - 3*maxValue] will be more probable (see example bellow) 
  // [+++++++++++++++|+++++++++++++++|+++++++++++++++|xxxxxxxxxx] - maxGeneratedCandidate
  // 0            maxValue      2*maxValue      3*maxValue   \-candidates to drop  
  for (var i = 0; i < size; ++i) { //Filling result arrray
    do { //Choosing only suitable candidate
      var candidate = generateNumber(maxValue.bitLength);
      if (candidate <= maxValue) { //candidate from [0, maxValue] is ok
        result[i] = candidate + lowerBound;
      }
      else {
        var maxGeneratedCandidate = my_math.pow2(maxValue.bitLength) - BigInt.from(1);
        if (candidate <= maxGeneratedCandidate - maxGeneratedCandidate % maxValue) {
          result[i] = candidate % maxValue + lowerBound;
        }
      }
    } while(result[i] == null);
  } //Filling result arrray
  return result;
}

BigInt generateNumber(int bitLength) {
  final pow2_32 = pow(2, 32);
  BigInt res = BigInt.from(0);
  var randGenerator = Random();
  var bitsFilled = 0;
  for (var i = 32; i <= bitLength; i += 32) {
    res |= BigInt.from(randGenerator.nextInt(pow2_32)) << (i - 32);
    bitsFilled = i;
  }
  res |= BigInt.from(randGenerator.nextInt(pow(2, bitLength - bitsFilled))) << bitsFilled;
  return res;
}
